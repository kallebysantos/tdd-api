FROM node:14.3-slim

RUN mkdir /app
WORKDIR /app

COPY package.json ./
RUN npm install --silent

COPY . /app/

RUN npm build