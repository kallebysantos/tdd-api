const userRepository = require('../repository/user.repository');
const cache = require('../repository/cache.repository');
const jwt = require('../utils/token.util');

const { TYPES, ERROR } = require('../utils/error.util');
const { compare } = require('../utils/encrypt.util');

const login = async (data) => {
  const user = await userRepository.findByEmail(data.email);
  const isPwdCheck = await compare(data.password, user.password);

  return new Promise((resolve, reject) => {
    if (isPwdCheck === false) {
      reject(new ERROR(TYPES.INVALID_CREDENTIALS));
    }
    const payload = {
      iss: jwt.DEFAULTS.ISSUER,
      sub: user.id,
      exp: jwt.DEFAULTS.LOGIN_EXP_TIME,
    };
    return jwt.generate(payload)
      .then((token) => resolve({ user, token }));
  });
};

const logout = (userId, token) => {
  userRepository.delCache(userId);
  cache.set(cache.PREFIX.blacklistToken(token), 1, jwt.DEFAULTS.LOGIN_EXP_TIME);
};

module.exports = {
  login,
  logout,
};
