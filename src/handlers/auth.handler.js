const ERROR = require('../utils/error.util');
const authenticate = require('../auth/authenticate.auth');
const { setCache } = require('../repository/user.repository');

const login = async (req, res) => {
  const userData = req.payload;

  return authenticate.login(userData)
    .then(({ user, token }) => {
      setCache(user);
      return token;
    })
    .then((token) => res.response({ token }).code(200))
    .catch((error) => ERROR.print(error));
};

const logout = (req, res) => {
  const { credentials, token } = req.auth;
  try {
    authenticate.logout(credentials.sub, token);

    return res.response({ credentials, token }).code(200);
  } catch (error) {
    return ERROR.print(error);
  }
};

module.exports = {
  login,
  logout,
};
