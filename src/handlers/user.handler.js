const UserRepository = require('../repository/user.repository');
const ERROR = require('../utils/error.util');

const encoder = (user) => ({
  id: user.id,
  name: user.name,
  email: user.email,
  roles: user.roles,
  links: {
    self: `/user/${user.id}`,
  },
  timestamp: {
    createdAt: user.createdAt,
    updatedAt: user.updatedAt,
  },
});

const get = async (req, res) => (
  UserRepository.findAll()
    .then((list) => list.map(encoder))
    .then((result) => res.response(result).code(200))
    .catch((error) => ERROR.print(error))
);

const getFiltered = async (req, res) => {
  const { filter, param } = req.params;
  const query = { [filter]: param };

  return UserRepository.findByParam(query)
    .then((list) => list.map(encoder))
    .then((result) => res.response(result).code(200))
    .catch((error) => ERROR.print(error));
};

const find = async (req, res) => {
  const { id } = req.params;

  return UserRepository.findById(id)
    .then((result) => res.response(encoder(result)).code(200))
    .catch((error) => ERROR.print(error));
};

const create = async (req, res) => {
  const data = req.payload;

  return UserRepository.create(data)
    .then((result) => res.response(encoder(result)).code(201))
    .catch((error) => ERROR.print(error));
};

const update = async (req, res) => {
  const data = req.payload;
  const { sub } = req.auth.credentials;

  return UserRepository.update(sub, data)
    .then((result) => res.response(encoder(result)).code(201))
    .catch((error) => ERROR.print(error));
};

module.exports = {
  get,
  getFiltered,
  find,
  create,
  update,
};
