require('dotenv').config();
require('./services/mongo.service');
const Server = require('./server');

const init = async () => {
  (await Server).start();
  console.log(`Server running on port ${process.env.APP_PORT} `);
};

init();
