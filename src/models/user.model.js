const mongoose = require('mongoose');

const { Schema } = mongoose;

const userSchema = new Schema({
  name: {
    type: String,
    min: 3,
    trim: true,
    required: 'Please supply a name',
  },
  email: {
    type: String,
    trim: true,
    unique: true,
    lowercase: true,
    required: 'Please supply an email address',
  },
  password: {
    type: String,
    min: 5,
    trim: true,
    required: 'Please supply a password',
  },
  roles: [{
    type: String,
    enum: ['Programmer', 'GameDesigner', '2dArtist', '3dArtist'],
  }],
}, { timestamps: {} });

module.exports = new mongoose.model('user', userSchema);
