const { redis } = require('../services/redis.service');

const set = (key, value, seconds) => {
  redis.set(key, value, 'EX', seconds);
};

const del = (key) => redis.del(key);

const exists = (key) => redis.exists(key);

const toCache = (value) => JSON.stringify(value);

const PREFIX = {
  user: (userId) => `userId:${userId}`,
  blacklistToken: (token) => `blacklistToken:${token}`,
};

module.exports = {
  set,
  del,
  exists,
  toCache,
  PREFIX,
};
