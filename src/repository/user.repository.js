const { isValidObjectId } = require('mongoose');
const UserModel = require('../models/user.model');
const cache = require('./cache.repository');
const { hash } = require('../utils/encrypt.util');
const { ERROR, TYPES } = require('../utils/error.util');
const { DEFAULTS } = require('../utils/token.util');

const remove = async (email) => UserModel.findOneAndDelete({ email });

const create = async (data) => {
  const userData = data;
  const itExist = await UserModel.exists({ email: userData.email });
  if (itExist) {
    throw new ERROR(TYPES.DUPLICATED_EMAIL);
  }
  userData.password = await hash(userData.password);
  const user = new UserModel(userData);

  return user.save()
    .catch(() => {
      throw new ERROR(TYPES.INVALID_VALUES);
    });
};

// https://discordapp.com/channels/463385102047641600/532702890632937483/784783288237293588
const update = async (id, data) => {
  if (isValidObjectId(id) === false) {
    throw new ERROR(TYPES.INVALID_QUERY);
  }
  const values = data;
  if ('password' in values) {
    values.password = await hash(values.password);
  }
  return UserModel.findByIdAndUpdate(id, data, { new: true })
    .catch(() => {
      throw new ERROR(TYPES.USER_NOT_FOUND);
    });
};

const findAll = async () => (
  UserModel.find()
    .catch(() => {
      throw new ERROR(TYPES.USER_NOT_FOUND);
    })
);

const findById = async (id) => {
  if (isValidObjectId(id) === false) {
    throw new ERROR(TYPES.INVALID_QUERY);
  }
  return UserModel.findById(id)
    .catch(() => {
      throw new ERROR(TYPES.USER_NOT_FOUND);
    });
};

const findByEmail = async (email) => (
  UserModel.findOne({ email })
    .catch(() => {
      throw new ERROR(TYPES.USER_NOT_FOUND);
    })
);

const findByParam = async (query) => (
  UserModel.find(query)
    .catch(() => {
      throw new ERROR(TYPES.USER_NOT_FOUND);
    })
);

const setCache = (user) => {
  cache.set(cache.PREFIX.user(user.id), cache.toCache(user), DEFAULTS.LOGIN_EXP_TIME);
};

const delCache = (id) => {
  if (isValidObjectId(id) === false) {
    throw new ERROR(TYPES.INVALID_QUERY);
  }
  cache.del(cache.PREFIX.user(id));
};

module.exports = {
  remove,
  create,
  update,
  findAll,
  findById,
  findByEmail,
  findByParam,
  delCache,
  setCache,
};
