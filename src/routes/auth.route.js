const authHandler = require('../handlers/auth.handler');
const authSchema = require('../schemas/auth.schema');

module.exports = [
  {
    method: 'POST',
    path: '/login',
    handler: authHandler.login,
    options: {
      auth: false,
      validate: {
        payload: authSchema.LOGIN,
      },
      description: 'Get authentication',
    },
  },
  {
    method: 'POST',
    path: '/logout',
    handler: authHandler.logout,
    options: {
      description: 'End a session',
    },
  },
];
