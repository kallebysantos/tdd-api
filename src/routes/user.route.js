const userHandler = require('../handlers/user.handler');
const userSchema = require('../schemas/user.schema');

module.exports = [
  {
    method: 'GET',
    path: '/user',
    handler: userHandler.get,
    options: {
      description: 'List all users',
    },
  },
  {
    method: 'GET',
    path: '/user/{id}',
    handler: userHandler.find,
    options: {
      validate: {
        params: userSchema.FIND,
      },
      description: 'Find an user based on ID',
    },
  },
  {
    method: 'GET',
    path: '/user/{filter}/{param}',
    handler: userHandler.getFiltered,
    options: {
      validate: {
        params: userSchema.FILTER,
      },
      description: 'Returns all users based on filter',
    },
  },
  {
    method: 'POST',
    path: '/user',
    handler: userHandler.create,
    options: {
      auth: false,
      validate: {
        payload: userSchema.CREATE,
      },
      description: 'Creates a new user',
    },
  },
  {
    method: 'PUT',
    path: '/user',
    handler: userHandler.update,
    options: {
      validate: {
        payload: userSchema.UPDATE,
      },
      description: 'Update an user data',
    },
  },
];
