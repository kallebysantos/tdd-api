const Joi = require('@hapi/joi');

const LOGIN = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(5).required(),
});

module.exports = {
  LOGIN,
};
