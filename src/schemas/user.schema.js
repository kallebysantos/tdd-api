const Joi = require('@hapi/joi');

const CREATE = Joi.object({
  name: Joi.string().min(3).required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(5).required(),
  roles: Joi.array().items(Joi.string()),
});

const UPDATE = Joi.object({
  name: Joi.string().min(3),
  password: Joi.string().min(5),
  roles: Joi.array().items(Joi.string()),
}).disallow({});

const FIND = Joi.object({
  id: Joi.string().alphanum().required(),
});

const FILTER = Joi.object({
  filter: Joi.string().valid('roles').required(),
  param: Joi.string().alphanum().min(3).required(),
});

module.exports = {
  CREATE,
  UPDATE,
  FIND,
  FILTER,
};
