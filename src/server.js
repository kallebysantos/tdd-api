const hapiAuthJWT2 = require('hapi-auth-jwt2');

const Hapi = require('@hapi/hapi');
const routes = require('./routes/index.route');
const authJWT = require('./strategies/auth-jwt.strategy');

const Server = async () => {
  const server = Hapi.server({
    port: 3000,
  });

  server.route(routes);

  // Protecao das rotas
  await server.register(hapiAuthJWT2);
  server.auth.strategy(authJWT.name, authJWT.schema, authJWT.options);
  server.auth.default(authJWT.name);
  return server;
};

module.exports = Server();
