const Redis = require('ioredis');

const connect = () => (
  new Redis({
    port: process.env.REDIS_PORT,
    host: process.env.REDIS_HOST,
  })
);

module.exports = {
  connection: null,

  get redis() {
    if (this.connection === null) {
      this.connection = connect();
    }
    return this.connection;
  },
};
