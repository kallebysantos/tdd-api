const cache = require('../repository/cache.repository');

module.exports = {
  name: 'auth-jwt',
  schema: 'jwt',
  options: {
    key: process.env.SECRET_KEY,
    validate: async (decoded, header) => {
      const isLogged = await cache.exists(cache.PREFIX.user(decoded.sub));
      const isBlackList = await cache.exists(cache.PREFIX.blacklistToken(header.auth.token));
      const isValid = isLogged * !isBlackList;

      return { isValid };
    },
  },
};
