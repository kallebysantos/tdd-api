const boom = require('@hapi/boom');

function ERROR(type) {
  this.error = new Error();
  this.func = type;
}

const TYPES = {
  INVALID_VALUES: () => boom.badData('Invalid values'),
  DUPLICATED_EMAIL: () => boom.badData('This user already exists'),
  USER_NOT_FOUND: () => boom.badData('This user not exists'),
  INVALID_QUERY: () => boom.badData('Invalid parammeter'),
  INVALID_CREDENTIALS: () => boom.badData('Invalid credentials'),
  FAILED_TOKEN: () => boom.badImplementation('Token creation error'),
};

const print = (error) => {
  if (typeof (error.func) === 'function') {
    return error.func();
  }
  return boom.badImplementation(error);
};

module.exports = {
  ERROR,
  TYPES,
  print,
};
