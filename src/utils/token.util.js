const JWT = require('jsonwebtoken');
const { TYPES, ERROR } = require('./error.util');

const DEFAULTS = {
  ISSUER: 'tdd-api',
  SECRET_KEY: process.env.SECRET_KEY,
  BASE_TIME: Math.floor(Date.now() / 1000),
  LOGIN_EXP_TIME: Math.floor(Date.now() / 1000) + 3600,
};

const generate = (payload, secretKey = DEFAULTS.SECRET_KEY) => (
  new Promise((resolve) => {
    JWT.sign(payload, secretKey, (error, token) => {
      if (error) {
        throw new ERROR(TYPES.FAILED_TOKEN);
      }

      resolve(token);
    });
  })
);

const validate = (token, secretKey = DEFAULTS.SECRET_KEY) => (
  new Promise((resolve) => (
    resolve(JWT.verify(token, secretKey, (error, payload) => {
      if (error) {
        return false;
      }

      return payload;
    }))
  ))
);

const decode = (token) => (
  JWT.decode(token)
);

module.exports = {
  DEFAULTS,
  generate,
  validate,
  decode,
};
