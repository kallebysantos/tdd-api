#!/bin/bash

LIGHT_CYAN='\033[1;36m'
CYAN='\033[0;36m'
GREEN='\033[1;32m'
PURPLE='\033[1;33m' 
NC='\033[0m' # No Color

echo -e "${PURPLE}Iniciando containers${NC}"
docker-compose -f docker-compose-tests.yml up -d
clear

#echo -e "${LIGHT_CYAN}Configurando aplicação${CYAN}"
echo -e "${GREEN}Executando testes automatizados${NC}"
docker logs -f tdd-app-test

echo -e "${PURPLE}Finalizando containers${NC}"
docker-compose down