const {
  it, routes, Server, routes: { status }, repository: { user },
} = require('../helpers/helper.spec');

it.serial.beforeEach('Populating database', async (exec) => {
  const data = await user.create();

  const share = exec;
  share.context.data = data;
});

it.serial.beforeEach('Getting authentication', async (exec) => {
  const req = { ...routes.auth.login };
  const { email, password } = user.schema;

  req.payload = { email, password };
  const { token } = await (await Server).inject(req)
    .then((res) => res.result);

  const share = exec;
  share.context.token = token;
});

it.serial.afterEach.always('Cleaning session', async (exec) => {
  const req = { ...routes.auth.logout };
  const { token } = exec.context;

  req.headers.authorization = `Bearer ${token}`;
  return (await Server).inject(req)
    .then((res) => exec.is(res.statusCode, status.SUCCESS));
});

it.serial.afterEach.always('Cleaning database', async () => {
  await user.clear();
});

it.serial('Should send a empty token to a protected route', async (expect) => {
  const req = { ...routes.user.get };

  return (await Server).inject(req)
    .then((res) => {
      expect.is(res.statusCode, status.UNAUTHORIZED);
    });
});

it.serial('Should be denied by a protected route', async (expect) => {
  const req = { ...routes.user.get };
  const token = 'abcAbC.ABCabc123.123abcABC';
  req.headers.authorization = `Bearer ${token}`;

  return (await Server).inject(req)
    .then((res) => {
      expect.is(res.statusCode, status.UNAUTHORIZED);
    });
});

it.serial('Should access a protected route', async (expect) => {
  const reqAuth = { ...routes.user.get };
  const { token } = expect.context;

  reqAuth.headers.authorization = `Bearer ${token}`;
  return (await Server).inject(reqAuth)
    .then((res) => {
      expect.is(res.statusCode, status.SUCCESS);
    });
});
