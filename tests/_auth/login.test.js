const {
  it,
  jwt,
  routes,
  Server,
  routes: { status },
  repository: { user: User },
} = require('../helpers/helper.spec');

const data = User.schema;

it.beforeEach(async () => {
  await User.create();
});

it.afterEach.always(async () => {
  await User.clear();
});

it.serial('Should send an empty payload', async (expect) => {
  const request = { ...routes.auth.login };

  return (await Server).inject(request)
    .then((res) => {
      expect.is(res.statusCode, status.BAD_REQUEST);
    });
});

it.serial('Should send an invalid payload', async (expect) => {
  const req = { ...routes.auth.login };
  req.payload = {
    email: 'example@.com',
    password: '123',
  };

  return (await Server).inject(req)
    .then((res) => {
      expect.is(res.statusCode, status.BAD_REQUEST);
    });
});

it.serial('Should send a wrong password', async (expect) => {
  const req = { ...routes.auth.login };
  const { email } = data;
  req.payload = {
    email,
    password: 'ABC-12345',
  };

  return (await Server).inject(req)
    .then((res) => {
      expect.is(res.statusCode, status.BAD_DATA);
    });
});

it.serial('Should be sucessfully logged', async (expect) => {
  const req = { ...routes.auth.login };
  const { email, password } = data;
  req.payload = { email, password };

  return (await Server).inject(req)
    .then(async (res) => {
      const { token } = res.result;
      const isValid = await jwt.validate(token);

      expect.is(res.statusCode, status.SUCCESS);
      expect.deepEqual(jwt.decode(token), isValid);
    });
});
