const {
  it, Server, jwt, routes, routes: { status }, repository: { user },
} = require('../helpers/helper.spec');

it.beforeEach(async () => {
  await user.create();
});

it.afterEach.always(async () => {
  await user.clear();
});

it.serial('Should send an empty data', async (expect) => {
  const req = { ...routes.auth.logout };

  return (await Server).inject(req)
    .then((res) => {
      expect.is(res.statusCode, status.UNAUTHORIZED);
    });
});

it.serial('Should send a not logged token', async (expect) => {
  const { _id } = await user.find();
  const token = await jwt.generate({ _id });

  const req = { ...routes.auth.logout };
  req.headers.authorization = `Bearer ${token}`;

  return (await Server).inject(req)
    .then((res) => {
      expect.is(res.statusCode, status.UNAUTHORIZED);
    });
});

it.serial('Should be sucessfully logout', async (expect) => {
  const reqLogin = { ...routes.auth.login };
  const { email, password } = user.schema;

  reqLogin.payload = { email, password };
  const token = await (await Server).inject(reqLogin)
    .then((res) => (res.result.token));

  const reqLogout = { ...routes.auth.logout };
  reqLogout.headers.authorization = `Bearer ${token}`;

  return (await Server).inject(reqLogout)
    .then((res) => {
      expect.is(res.statusCode, status.SUCCESS);
    });
});
