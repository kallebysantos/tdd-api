const {
  it, routes, Server, routes: { status }, repository: { user },
} = require('../helpers/helper.spec');

/* Tests Setup */

it.serial.beforeEach('Populating database', async (exec) => {
  const data = await user.create();

  const share = exec;
  share.context.data = data;
});

it.serial.beforeEach('Getting authentication', async (exec) => {
  const req = { ...routes.auth.login };
  const { email, password } = user.schema;
  req.payload = { email, password };

  const token = await (await Server).inject(req)
    .then((res) => (res.result.token));

  const share = exec;
  share.context.token = token;
});

it.serial.afterEach.always('Cleaning session, logout', async (exec) => {
  const req = { ...routes.auth.logout };
  const { token } = exec.context;

  req.headers.authorization = `Bearer ${token}`;

  return (await Server).inject(req)
    .then((res) => {
      exec.is(res.statusCode, status.SUCCESS);
    });
});

it.serial.afterEach.always('Cleaning database', async () => {
  await user.clear();
});

/* Implementation */

it.serial('Should be unauthorized', async (expect) => {
  const req = { ...routes.user.find };
  const { data } = expect.context;

  req.url += data.id;
  return (await Server).inject(req)
    .then((res) => expect.is(res.statusCode, status.UNAUTHORIZED));
});

it.serial('Should send an empty query', async (expect) => {
  const req = { ...routes.user.find };
  const { token } = expect.context;

  req.headers.authorization = `Bearer ${token}`;
  return (await Server).inject(req)
    .then((res) => expect.is(res.statusCode, status.NOT_FOUND));
});

it.serial('Should send an invalid query', async (expect) => {
  const req = { ...routes.user.find };
  const { token } = expect.context;

  req.url += '<script=MaliciusHacking>';
  req.headers.authorization = `Bearer ${token}`;

  return (await Server).inject(req)
    .then((res) => expect.is(res.statusCode, status.BAD_REQUEST));
});

it.serial('Should be valid but not find', async (expect) => {
  const req = { ...routes.user.find };
  const { token } = expect.context;

  req.url += '123';
  req.headers.authorization = `Bearer ${token}`;

  return (await Server).inject(req)
    .then(async (res) => expect.is(res.statusCode, status.BAD_DATA));
});

it.serial('Should be valid and find', async (expect) => {
  const req = { ...routes.user.find };
  const { token, data } = expect.context;

  req.url += data.id;
  req.headers.authorization = `Bearer ${token}`;

  return (await Server).inject(req)
    .then(async (res) => {
      const query = await user.find();
      const result = user.encoder(query);

      expect.deepEqual(res.result, result);
      expect.is(res.statusCode, status.SUCCESS);
    });
});
