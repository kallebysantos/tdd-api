const {
  it, routes, routes: { status }, repository: { user }, Server,
} = require('../helpers/helper.spec');

it.serial.beforeEach('Populating database', async (exec) => {
  const { context } = exec;
  context.data = await user.create();
  return context;
});

it.serial.beforeEach('Getting authentication', async (exec) => {
  const { context } = exec;
  const req = { ...routes.auth.login };
  const { email, password } = user.schema;

  req.payload = { email, password };
  context.token = await (await Server).inject(req)
    .then((res) => (res.result.token));
  return context;
});

it.serial.afterEach.always('Cleaning session. logout', async (exec) => {
  const req = { ...routes.auth.logout };
  const { token } = exec.context;

  req.headers.authorization = `Bearer ${token}`;
  return (await Server).inject(req)
    .then((res) => {
      exec.is(res.statusCode, status.SUCCESS);
    });
});

it.serial.afterEach.always('Cleaning database', async () => {
  await user.clear();
});

it.serial('Should be unauthorized', async (expect) => {
  const req = { ...routes.user.get };

  return (await Server).inject(req)
    .then((res) => expect.is(res.statusCode, status.UNAUTHORIZED));
});

it.serial('Should get a list with all users', async (expect) => {
  const req = { ...routes.user.get };
  const { token } = expect.context;

  req.headers.authorization = `Bearer ${token}`;
  return (await Server).inject(req)
    .then(async (res) => {
      const list = await user.getAll();
      const result = list.map(user.encoder);

      expect.deepEqual(res.result, result);
      expect.is(res.statusCode, status.SUCCESS);
    });
});
