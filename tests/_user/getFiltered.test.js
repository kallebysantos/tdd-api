const {
  it, routes, Server, routes: { status }, repository: { user },
} = require('../helpers/helper.spec');

require('../helpers/helper.spec');

/* Tests Setup */

it.serial.beforeEach('Populating database', async (exec) => {
  const share = exec;
  share.context.data = await user.create();
  share.context.users = await user.populate(5);
  return share;
});

it.serial.beforeEach('Getting authentication', async (exec) => {
  const req = { ...routes.auth.login };
  const { email, password } = user.schema;
  req.payload = { email, password };

  const share = exec;
  share.context.token = await (await Server).inject(req)
    .then((res) => res.result.token);

  return share;
});

it.serial.afterEach.always('Cleaning session', async (exec) => {
  const req = { ...routes.auth.logout };
  const { token } = exec.context;

  req.headers.authorization = `Bearer ${token}`;
  return (await Server).inject(req)
    .then((res) => exec.is(res.statusCode, status.SUCCESS));
});

it.serial.afterEach.always('Cleaning database', async () => {
  await user.clear();
});

/* Tests Implementation */

it.serial('Should be unauthorized', async (expect) => {
  const req = { ...routes.user.getFiltered('role', 'programmer') };

  return (await Server).inject(req)
    .then((res) => expect.is(res.statusCode, status.UNAUTHORIZED));
});

it.serial('Should send an invalid filter', async (expect) => {
  const req = { ...routes.user.getFiltered('Abobrinhas', 'programmer', expect.context.token) };

  return (await Server).inject(req)
    .then((res) => {
      expect.is(res.statusCode, status.BAD_REQUEST);
    });
});

it.serial('Should send an invalid parammeter', async (expect) => {
  const req = { ...routes.user.getFiltered('roles', '<script>hacker??#$') };
  const { token } = expect.context;

  req.headers.authorization = `Bearer ${token}`;
  return (await Server).inject(req)
    .then((res) => expect.is(res.statusCode, status.BAD_REQUEST));
});

it.serial('Should be authorized and get a list of users', async (expect) => {
  const { token, data } = expect.context;
  const req = { ...routes.user.getFiltered('roles', data.roles, token) };

  return (await Server).inject(req)
    .then(async (res) => {
      const filter = { roles: data.roles };
      const query = await user.findByParam(filter);
      const result = query.map(user.encoder);

      expect.deepEqual(res.result, result);
      expect.is(res.statusCode, status.SUCCESS);
    });
});
