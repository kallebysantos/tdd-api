const {
  it,
  routes,
  Server,
  routes: { status },
  repository: { user },
} = require('../helpers/helper.spec');

it('Should send an empty payload', async (expect) => {
  const req = { ...routes.user.create };

  return (await Server).inject(req)
    .then((res) => {
      expect.is(res.statusCode, status.BAD_REQUEST);
    });
});

it('Should send an invalid payload', async (expect) => {
  const req = { ...routes.user.create };
  req.payload = {
    name: 'te',
    email: 'test.example.com',
    password: '123',
  };

  return (await Server).inject(req)
    .then((res) => {
      expect.is(res.statusCode, status.BAD_REQUEST);
    });
});

it.serial('Should be successfully created', async (expect) => {
  const req = { ...routes.user.create };
  req.payload = user.schema;

  return (await Server).inject(req)
    .then(async (res) => expect.is(res.statusCode, status.CREATED));
});

it.serial('Should send an already in use email', async (expect) => {
  const req = { ...routes.user.create };
  req.payload = { ...user.schema };

  return (await Server).inject(req)
    .then(async (res) => {
      expect.is(res.statusCode, status.ALREADY_EXISTS);
    });
});
