const {
  it, routes, Server, routes: { status }, repository: { user },
} = require('../helpers/helper.spec');

it.serial.beforeEach('Populating database', async () => {
  await user.create();
});

it.serial.beforeEach('Getting auth token', async (exec) => {
  const req = { ...routes.auth.login };
  const { email, password } = user.schema;

  req.payload = { email, password };
  const token = await (await Server).inject(req)
    .then((res) => (res.result.token));

  const share = exec;
  share.context = token;
});

it.afterEach.always('Cleaning database', async () => {
  await user.clear();
});

it.serial('Should be unauthorized', async (expect) => {
  const req = { ...routes.user.update };

  return (await Server).inject(req)
    .then((res) => {
      expect.is(res.statusCode, status.UNAUTHORIZED);
    });
});

it.serial('Should send empty data', async (expect) => {
  const req = { ...routes.user.update };
  const token = expect.context;

  req.headers.authorization = `Bearer ${token}`;
  return (await Server).inject(req)
    .then((res) => {
      expect.is(res.statusCode, status.BAD_REQUEST);
    });
});

it.serial('Should send an invalid data', async (expect) => {
  const req = { ...routes.user.update };
  const token = expect.context;

  const { name } = user.schema;
  req.payload = { name, password: '123' };

  req.headers.authorization = `Bearer ${token}`;
  return (await Server).inject(req)
    .then((res) => {
      expect.is(res.statusCode, status.BAD_REQUEST);
    });
});

it.serial('Should sucessfully update an user', async (expect) => {
  const req = { ...routes.user.update };
  const token = expect.context;

  const data = { name: 'Test', password: 'test123' };
  req.payload = { ...data };

  req.headers.authorization = `Bearer ${token}`;
  return (await Server).inject(req)
    .then(async (res) => {
      const query = await user.find();
      const result = user.encoder(query);

      expect.deepEqual(res.result, result);
      expect.is(res.result.name, data.name);
      expect.is(res.statusCode, status.CREATED);
    });
});
