const it = require('ava');
const jwt = require('../../src/utils/token.util');

const DEFAULT = {
  valid_key: 'key123',
  invalid_key: 'example',
};

it('Invalid token', async (expect) => {
  const token = await jwt.generate({ name: 'example' }, DEFAULT.valid_key);
  const isValid = await jwt.validate(token, DEFAULT.invalid_key);

  return expect.false(isValid);
});

it('Valid token', async (expect) => {
  const token = await jwt.generate({ name: 'example' }, DEFAULT.valid_key);
  const isValid = await jwt.validate(token, DEFAULT.valid_key);

  return expect.deepEqual(jwt.decode(token), isValid);
});
