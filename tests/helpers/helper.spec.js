require('dotenv').config();

const it = require('ava');
const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');

const jwt = require('../../src/utils/token.util');
const Server = require('../../src/server');

const routes = require('./routes/helper.route.spec');
const repository = require('./repositories/helper.repository.spec');

const Memory = new MongoMemoryServer();

// Start a temporary memory cache Database
it.before(async () => {
  const uri = await Memory.getUri();

  await mongoose.connect(uri, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
});

// Closes memory cache
it.after.always(async () => {
  mongoose.disconnect();
  Memory.stop();
});

module.exports = {
  it,
  jwt,
  Server,
  routes,
  repository,
  Memory,
  mongoose,
};
