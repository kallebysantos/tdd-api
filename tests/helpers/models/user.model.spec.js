const rand = (array) => (
  array[Math.floor((Math.random() * array.length))]
);

const FIRST_NAME = ['Kalleby', 'Veronica', 'Joãozinho', 'Mariazinha'];
const LAST_NAME = ['Santos', 'Plesca', 'Batatinha', 'Rodrigues'];
const DOMAIN = ['gmail.com', 'hotmail.com', 'outlook.com', 'yahoo.com'];
const SKILLS = ['Programmer', 'GameDesigner', '2dArtist', '3dArtist'];
const random = (value) => Math.floor(Math.random() * value);

const RandomSkill = () => rand(SKILLS);

const RandomSchema = () => ({
  name: `${rand(FIRST_NAME)} ${rand(LAST_NAME)}`,
  email: `${rand(FIRST_NAME)}${Math.floor(random(100) * random(25))}.${rand(FIRST_NAME)}@${rand(DOMAIN)}`,
  password: `${rand(FIRST_NAME)}${Math.floor(Math.random() * 10)}`,
  roles: [RandomSkill()],
});

module.exports = {
  RandomSkill,
  RandomSchema,
};
