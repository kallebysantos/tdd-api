const FakeModel = require('../models/user.model.spec');
const Model = require('../../../src/models/user.model');
const { hash } = require('../../../src/utils/encrypt.util');

const schema = FakeModel.RandomSchema();

const encoder = (user) => ({
  id: user.id,
  name: user.name,
  email: user.email,
  roles: user.roles,
  links: {
    self: `/user/${user.id}`,
  },
  timestamp: {
    createdAt: user.createdAt,
    updatedAt: user.updatedAt,
  },
});

const find = async (data = schema) => {
  const { email } = data;
  const user = await Model.findOne({ email });
  return user;
};

const findByParam = async (query) => Model.find(query);

const getAll = async () => Model.find();

const clear = async () => Model.deleteMany();

const create = async (data = schema, encrypt = true) => {
  const user = { ...data };
  user.password = (encrypt) ? await hash(data.password) : data.password;

  const result = new Model(user);
  return result.save();
};

const populate = async (count, list = []) => {
  const data = FakeModel.RandomSchema();
  data.password = await hash(data.password);
  const user = await new Model(data).save();

  list.push(user);
  return (count <= 1) ? list : populate(count - 1, list);
};

module.exports = {
  Model,
  FakeModel,
  schema,
  find,
  findByParam,
  encoder,
  getAll,
  clear,
  create,
  populate,

};
