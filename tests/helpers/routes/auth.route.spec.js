module.exports = {
  login: {
    method: 'POST',
    url: '/login',
    payload: {},
  },
  logout: {
    method: 'POST',
    url: '/logout',
    headers: {
      authorization: '',
    },
  },
};
