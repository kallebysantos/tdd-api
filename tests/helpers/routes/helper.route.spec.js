const authRoutes = require('./auth.route.spec');
const userRoutes = require('./user.route.spec');

const status = {
  SUCCESS: 200,
  CREATED: 201,
  UNAUTHORIZED: 401,
  BAD_REQUEST: 400,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  BAD_DATA: 422,
  ALREADY_EXISTS: 422,
};

module.exports = {
  status,
  auth: { ...authRoutes },
  user: { ...userRoutes },
};
