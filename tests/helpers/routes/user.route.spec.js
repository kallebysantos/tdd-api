module.exports = {
  get: {
    method: 'GET',
    url: '/user',
    payload: {},
    headers: {
      authorization: '',
    },
  },
  getFiltered: (filter, param, auth) => ({
    method: 'GET',
    url: `/user/${filter}/${param}`,
    headers: {
      authorization: `Bearer ${auth}`,
    },
  }),
  find: {
    method: 'GET',
    url: '/user/',
    payload: {},
    headers: {
      authorization: '',
    },
  },
  create: {
    method: 'POST',
    url: '/user',
    payload: {},
  },
  update: {
    method: 'PUT',
    url: '/user',
    payload: {},
    headers: {
      authorization: '',
    },
  },
};
